Gstreamer Test Plugins 
======================

Build
-----
Build and test Gstreamer Cuda sink:
```
cd gst-cudafilter
./autogen.sh
mkdir ../out
GST_CUDA_BUILD_DIR=`readlink -f ../out`
./configure --prefix=$GST_CUDA_BUILD_DIR --with-cuda=/usr/local/cuda-9.0
make && make install
GST_PLUGIN_PATH=$GST_CUDA_BUILD_DIR gst-launch-1.0 videotestsrc ! 'video/x-raw,format=BGRx' ! cudasink
```


Links
-----
[configure.ac for CUDA](http://wili.cc/blog/cuda-m4.html)  
[autotools, CUDA](https://tschoonj.github.io/blog/2014/05/10/building-a-cuda-device-function-library-with-autotools/)  
[Intel, Gstreamer Implementation Overview](https://software.intel.com/en-us/sample-gstreamer-openvx-interoperability-gstreamer-plugin-implementation-overview)  
[PyCUDA Install, Linux](https://wiki.tiker.net/PyCuda/Installation/Linux)  
[PyCUDA Examples](https://wiki.tiker.net/PyCuda/Examples)  
[PyCUDA Performance](https://habr.com/ru/post/317328/)  
[Gstreamer, Python Examples](https://github.com/GStreamer/gst-python/tree/master/examples)  
