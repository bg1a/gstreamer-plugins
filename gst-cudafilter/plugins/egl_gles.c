#include <GLES3/gl31.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>

#include "egl_gles.h"
#include "egl_x11_win.h"
#include "egl_gles_cuda.cuh"
#include "common.cuh"

#define GST_CAT_DEFAULT GST_CAT_CUDA_EGL_GLES
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);

static const GLfloat vertices[] = {
    //Vertex coord.     Texture coord.
    -1.0, -1.0,         0.0, 1.0,
    -1.0,  1.0,         0.0, 0.0,
     1.0, -1.0,         1.0, 1.0,
     1.0,  1.0,         1.0, 0.0,
};

static const char *v_shader = 
"#version 330 core \n"
"layout (location=0) in vec2 vPos; \n"
"layout (location=1) in vec2 tPos; \n"
"out vec2 texcoord; \n"
"void main(void) \n"
"{ \n"
"  gl_Position = vec4(vPos, 0.0, 1.0); \n"
"  texcoord = tPos; \n"
"} \n";

static const char *frag_shader =
"#version 330 core \n"
"in vec2 texcoord; \n"
"uniform sampler2D tex; \n"
"out vec4 FragColor; \n"
"void main(void) \n"
"{ \n"
"  FragColor = texture(tex, texcoord); \n"
"} \n";

typedef struct _gst_spinlock
{
#define GST_SPINLOCK_UNLOCKED_VAL   0
#define GST_SPINLOCK_LOCKED_VAL     1

    int flag;
}gst_spinlock;

static inline void
gst_spinlock_init(gst_spinlock *lock)
{
    g_atomic_int_set(&lock->flag, GST_SPINLOCK_UNLOCKED_VAL);
}

static inline void
gst_spinlock_lock(gst_spinlock *lock)
{
    int locked_val = GST_SPINLOCK_LOCKED_VAL;
    int unlocked_val = GST_SPINLOCK_UNLOCKED_VAL;

    while(1){
        if(g_atomic_int_compare_and_exchange(&lock->flag, unlocked_val, locked_val)) break;
    }
}

static inline void
gst_spinlock_unlock(gst_spinlock *lock)
{
    g_atomic_int_set(&lock->flag, GST_SPINLOCK_UNLOCKED_VAL);
}

struct _GstCudaEglGles
{
    GstCudaEglX11Win *wHandle;

    GstVideoInfo *info;

    GLuint vao;
    GLuint vbo;
    GLuint program;
    GLuint vertex_shader;
    GLuint frag_shader;

    GLuint tex;

    EglGlesCudaContext *egl_cuda_ctx;

    unsigned char *thread_buf;
    gboolean thread_buf_need_display;
    gst_spinlock thread_buf_lock;

    GMutex mutex;
};

#define EGLES_MUTEX_LOCK(_egles) g_mutex_lock(&((_egles)->mutex))
#define EGLES_MUTEX_UNLOCK(_egles) g_mutex_unlock(&((_egles)->mutex))

static GLint
get_opengl_vformat(GstVideoInfo *info)
{
    GLint gl_vformat = GL_RGBA;
    /* TODO: convert Gstreamer video format to compatible OpenGL pixel format */
    return gl_vformat;
}


static void 
init_opengl_shaders(GstCudaEglGles *egles)
{
    GLuint vs, fs, program;

    vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, (const GLchar **) &v_shader, NULL);
    glCompileShader(vs);
    
    fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, (const GLchar **) &frag_shader, NULL);
    glCompileShader(fs);
    
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glUseProgram(program);

    egles->program = program;
    egles->vertex_shader = vs;
    egles->frag_shader = fs;
}

static void
remove_opengl_shader(GstCudaEglGles *egles)
{
    if(egles){
        glUseProgram(0);

        if(egles->program){
            if(egles->vertex_shader){
                glDetachShader(egles->program, egles->vertex_shader);
                glDeleteShader(egles->vertex_shader);
                egles->vertex_shader = 0;
            }
            if(egles->frag_shader){
                glDetachShader(egles->program, egles->frag_shader);
                glDeleteShader(egles->frag_shader);
                egles->frag_shader = 0;
            }

            glDeleteProgram(egles->program);
            egles->program = 0;
        }
    }
}

static void
init_opengl_attributes(GstCudaEglGles *egles)
{
    glGenVertexArrays(1, &egles->vao);
    glBindVertexArray(egles->vao);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &egles->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, egles->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(2*sizeof(float)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

static void
remove_opengl_attributes(GstCudaEglGles *egles)
{
    if(egles){
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

        glDeleteBuffers(1, &egles->vbo);
        egles->vbo = 0;
        glGenVertexArrays(1, &egles->vao);
        egles->vao = 0;
    }
}

static void
init_opengl_textures(GstCudaEglGles *egles)
{
    GstVideoInfo *info = egles->info;
    GLint vformat = get_opengl_vformat(info);
    GLsizei img_width = GST_VIDEO_INFO_WIDTH(info);
    GLsizei img_height = GST_VIDEO_INFO_HEIGHT(info);

    glGenTextures(1, &egles->tex);
    glBindTexture(GL_TEXTURE_2D, egles->tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glTexImage2D(GL_TEXTURE_2D, 0, vformat, img_width, img_height, 
            0, vformat, GL_UNSIGNED_BYTE, NULL); 
}

static void
remove_opengl_textures(GstCudaEglGles *egles)
{
    if(egles){
        glBindTexture(GL_TEXTURE_2D, 0); 
        glDeleteTextures(1, &egles->tex);
        egles->tex = 0;
    }
}

static void
_render_init_callback_fn(void *egles_data)
{
    GstCudaEglGles *egles = (GstCudaEglGles *)egles_data;

    init_opengl_shaders(egles);
    init_opengl_attributes(egles);
    init_opengl_textures(egles);
    egles->egl_cuda_ctx = egl_gles_cuda_context_create(); 
    egl_gles_cuda_context_register_image(egles->egl_cuda_ctx, &egles->tex);
}

static void
_render_callback_fn(void *egles_data)
{
    GstCudaEglGles *egles = (GstCudaEglGles *)egles_data;
    int frame_data_size = GST_VIDEO_INFO_SIZE(egles->info);

    if(egles){
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH);
        glDisable(GL_LIGHTING);
        glClear(GL_COLOR_BUFFER_BIT);

        gst_spinlock_lock(&egles->thread_buf_lock);  
        if(egles->thread_buf_need_display){
            egl_gles_cuda_context_copy_to_image(egles->egl_cuda_ctx, 
                    egles->thread_buf, frame_data_size);
            egles->thread_buf_need_display = FALSE;
        }
        gst_spinlock_unlock(&egles->thread_buf_lock);  

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }else{
        GST_ERROR("Callback triggered with bad data!");
    }
}

static void
_render_close_callback_fn(void *egles_data)
{
    GstCudaEglGles *egles = (GstCudaEglGles *)egles_data;

    if(egles){
        remove_opengl_textures(egles);
        remove_opengl_attributes(egles);
        remove_opengl_shader(egles);

        if(egles->egl_cuda_ctx){
            egl_gles_cuda_context_destroy(egles->egl_cuda_ctx);
            egles->egl_cuda_ctx = NULL;
        }
    }else{
        GST_ERROR("Callback triggered with bad data!");
    }
}

gboolean
gst_egl_gles_display_frame(GstCudaEglGles *egles, GstBuffer *buf)
{
    gboolean ret = TRUE;

    if(egles && buf){
        GstVideoFrame vframe;
        unsigned char *cuda_data = NULL;
        gboolean need_wait = FALSE;
        //int cuda_data_size = 0;

        EGLES_MUTEX_LOCK(egles);
        gst_video_frame_map(&vframe, egles->info, buf, GST_MAP_READ);
        cuda_data = GST_VIDEO_FRAME_PLANE_DATA(&vframe, 0);
        //cuda_data_size = GST_VIDEO_INFO_SIZE(egles->info); 

        gst_spinlock_lock(&egles->thread_buf_lock);
        if(TRUE == egles->thread_buf_need_display){
            need_wait = TRUE;
        }
        gst_spinlock_unlock(&egles->thread_buf_lock);

        if(need_wait){
            g_usleep(2000);
        }

        gst_spinlock_lock(&egles->thread_buf_lock);
        if(TRUE == egles->thread_buf_need_display){
            GST_ERROR("Rewriting buffer that was not rendered!");
        }
        //cuda_copy_device_to_device(egles->thread_buf, cuda_data, cuda_data_size);
        cuda_bgra_to_rgba(egles->thread_buf, cuda_data, 
                GST_VIDEO_INFO_WIDTH(egles->info), GST_VIDEO_INFO_HEIGHT(egles->info));
        egles->thread_buf_need_display = TRUE;
        gst_spinlock_unlock(&egles->thread_buf_lock);

        gst_video_frame_unmap(&vframe);

        EGLES_MUTEX_UNLOCK(egles);
    }else{
        GST_ERROR("Bad argument!");
    }

    return ret;
}

gboolean
gst_egl_gles_start(GstCudaEglGles *egles, const GstVideoInfo *info, guintptr parent_win,
        int xpos, int ypos)
{
    gboolean ret = FALSE;
    
    if(egles && info){
        GstCudaEglX11WinRenderCallback render_init_cb = {
            .render_fn = _render_init_callback_fn,
            .data = egles,
        };
        GstCudaEglX11WinRenderCallback render_cb = {
            .render_fn = _render_callback_fn,
            .data = egles,
        };
        GstCudaEglX11WinRenderCallback render_close_cb = {
            .render_fn = _render_close_callback_fn,
            .data = egles,
        };
        int width = GST_VIDEO_INFO_WIDTH(info);
        int height = GST_VIDEO_INFO_HEIGHT(info);
       
        EGLES_MUTEX_LOCK(egles);
        egles->thread_buf = gst_cuda_alloc_device(GST_VIDEO_INFO_SIZE(info));
        GST_INFO("Allocated thread buffer %p, size: %"G_GSIZE_FORMAT, 
                egles->thread_buf, GST_VIDEO_INFO_SIZE(info));
        egles->info = gst_video_info_copy(info);
        ret = gst_egl_x11_win_display(egles->wHandle, parent_win, 
            &render_init_cb, &render_cb, &render_close_cb,
                xpos, ypos, width, height);
        EGLES_MUTEX_UNLOCK(egles);
    }else{
        GST_ERROR("Bad argument!");
    }

    return ret;
}

static void
_gst_egl_gles_stop(GstCudaEglGles *egles)
{
    if(egles){
        gst_egl_x11_win_close(egles->wHandle);

        if(egles->info){
            gst_video_info_free(egles->info);
            egles->info = NULL;
        }
        if(egles->thread_buf){
            gst_cuda_free(egles->thread_buf);
            egles->thread_buf = NULL;
        }
    }
}

void
gst_egl_gles_stop(GstCudaEglGles *egles)
{
    if(egles){
        EGLES_MUTEX_LOCK(egles);
        _gst_egl_gles_stop(egles); 
        EGLES_MUTEX_UNLOCK(egles);
    }else{
        GST_ERROR("Bad argument!");
    }
}

void
gst_egl_gles_init(void)
{
    static volatile gsize init = 0;
    if(g_once_init_enter(&init)){
        GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "cudaeglgles", 0, "Cuda EGL GLES");    
    }
}

GstCudaEglGles *
gst_egl_gles_create(void)
{
    GstCudaEglGles *egles = NULL;

    gst_egl_gles_init();

    egles = g_new0(GstCudaEglGles, 1);
    
    if(egles){
        GstCudaEglX11Win *wHandle = gst_egl_x11_win_create();
        g_mutex_init(&egles->mutex); 

        if(wHandle){
            egles->wHandle = wHandle; 
            egles->info = NULL;
            egles->thread_buf = NULL;
            egles->thread_buf_need_display = FALSE;
            gst_spinlock_init(&egles->thread_buf_lock);
        }else{
            GST_ERROR("Failed to create EGL GLES!");
            g_free(egles);
            egles = NULL;
        }
    }else{
        GST_ERROR("Failed to allocate memory!");
    }

    return egles;
}

void gst_egl_gles_destroy(GstCudaEglGles *egles)
{
    if(egles){
        GstCudaEglX11Win *wHandle = egles->wHandle;

        EGLES_MUTEX_LOCK(egles);
        _gst_egl_gles_stop(egles);
        egles->wHandle = NULL;
        EGLES_MUTEX_UNLOCK(egles);

        gst_egl_x11_win_destroy(wHandle);

        g_free(egles);
    }
}
