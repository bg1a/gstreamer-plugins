#include <stdio.h>
#include <stdlib.h>

#include "egl_gles_cuda.cuh"

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>

/* 
 * Note: GLib extensions have not been tested with Cuda.
 * So, avoid of using GLib. Use STDLIB instead
 */

#define checkCudaErrors(err) __checkCudaErrors(err, __LINE__)
cudaError_t __checkCudaErrors(cudaError_t err, int linenum)
{
    if (cudaSuccess != err){
        fprintf(stderr, "line %d, checkCudaErrors: %s\n", linenum, cudaGetErrorString(err));
    }
    return err;
}

extern "C"
struct _EglGlesCudaContext
{
    struct cudaGraphicsResource *cu_rsrc;
    GLuint *tex;
};

extern "C" EglGlesCudaContext *
egl_gles_cuda_context_create(void)
{
    EglGlesCudaContext *ctx = NULL;

    ctx = (EglGlesCudaContext *)malloc(sizeof(*ctx));
    if(ctx){
        ctx->cu_rsrc = NULL;
        ctx->tex = NULL;
    }else{
        fprintf(stderr, "Failed to allocate memory in %s!\n", __func__);
    }

    return ctx;
}

extern "C" void
egl_gles_cuda_context_destroy(EglGlesCudaContext *egl_cuda_ctx)
{
    if(egl_cuda_ctx){
        free(egl_cuda_ctx);
    }
}

extern "C" int
egl_gles_cuda_context_register_image(EglGlesCudaContext *egl_cuda_ctx, GLuint *tex)
{
    cudaError_t cu_err = cudaSuccess;
    int ret = 0;

    if(egl_cuda_ctx && tex){
        egl_cuda_ctx->tex = tex;
        cu_err = checkCudaErrors(
                cudaGraphicsGLRegisterImage(&egl_cuda_ctx->cu_rsrc, *tex, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard)
                );
        if(cu_err != cudaSuccess){
            fprintf(stderr, "Failed to register an image!\n");
            ret = -1;
        }
    }else{
        fprintf(stderr, "Bad arguments in %s, egl_cuda_ctx %p, tex %p\n", __func__, egl_cuda_ctx, tex);
        ret = -1;
    } 

    return ret;
}

extern "C" int
egl_gles_cuda_context_copy_to_image(EglGlesCudaContext *egl_cuda_ctx,
        unsigned char *cuda_data, int cuda_data_size)
{
    cudaError_t cu_err = cudaSuccess;
    int ret = 0;

    if(egl_cuda_ctx && cuda_data && cuda_data_size){
        cudaArray *cu_rsrc_array;

        cu_err = checkCudaErrors(
                cudaGraphicsMapResources(1, &egl_cuda_ctx->cu_rsrc, 0)
                );

        if(cudaSuccess == cu_err){
            cu_err = checkCudaErrors(
                    cudaGraphicsSubResourceGetMappedArray(&cu_rsrc_array, egl_cuda_ctx->cu_rsrc, 0, 0)
                    );
            cu_err = checkCudaErrors(
                    cudaMemcpyToArray(cu_rsrc_array, 0, 0, cuda_data, cuda_data_size, cudaMemcpyDeviceToDevice)
                    );
            if(cudaSuccess != cu_err){
                fprintf(stderr, "Cuda memcpy failed in %s\n!", __func__);
            }
            cu_err = checkCudaErrors(
                    cudaGraphicsUnmapResources(1, &egl_cuda_ctx->cu_rsrc, 0)
                    );
        } 
    }else{
        fprintf(stderr, "Bad arguments in %s, egl_cuda_ctx %p, cuda_data %p, cuda_data_size %d\n",
                __func__, egl_cuda_ctx, cuda_data, cuda_data_size);
        ret = -1;
    }

    return ret;
}
