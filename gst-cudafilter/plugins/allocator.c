#include <unistd.h>

#include <gst/video/video.h>
#include "allocator.h"
#include "common.cuh"


/*
 * See:
 * gstreamer-1.14.4/tests/examples/memory/my-vidmem.c
 * gst-plugins-bad-1.14.4/sys/vdpau/gstvdpvideomemory.c
 * gst-plugins-bad-1.14.4/ext/vulkan/vkmemory.c
 * gst-plugins-bad-1.14.4/sys/kms/gstkmsallocator.c
 */

#define GST_CAT_DEFAULT GST_CAT_CUDA_MEMORY
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

G_DEFINE_TYPE_WITH_CODE (GstCudaAllocator, gst_cuda_allocator, GST_TYPE_ALLOCATOR,
        GST_DEBUG_CATEGORY_INIT (GST_CAT_DEFAULT, "cudaallocator", 0, 
            "Cuda Allocator"));

static struct _GstAllocator *_cuda_memory_allocator;

gboolean
gst_is_cuda_memory (GstMemory * mem)
{
    return gst_memory_is_type (mem, GST_CUDA_MEMORY_TYPE);
}

static GstMemory *
_gst_cuda_allocator_alloc(GstAllocator * allocator, gsize size, GstAllocationParams *params)
{
    GST_WARNING("Use gst_cuda_allocator_alloc to allocate from this allocator");
    return NULL;
}

static void
_gst_cuda_allocator_free(GstAllocator *allocator, GstMemory *mem)
{
    GstCudaMemory *cmem = (GstCudaMemory *)mem;

    GST_DEBUG("Free GstCudaMemory: %p", mem);
    GST_DEBUG("Cuda memory pointer: %"GST_PTR_FORMAT, cmem->data);
    gst_cuda_free(cmem->data);
    g_slice_free(GstCudaMemory, cmem);
}

static gpointer
_gst_cuda_memory_map(GstCudaMemory *mem, gsize maxsize, GstMapFlags flags)
{
    gpointer res = NULL;

    GST_DEBUG("Mapping Cuda Memory %p, size %"G_GSIZE_FORMAT, mem, maxsize);
    while(TRUE){
        if((res = g_atomic_pointer_get(&mem->data)) != NULL) break;
        
        res = gst_cuda_alloc(maxsize);
        GST_DEBUG("Cuda memory pointer: %"GST_PTR_FORMAT, res);

        if(g_atomic_pointer_compare_and_exchange(&mem->data, NULL, res)) break;
   
        gst_cuda_free(res);
    }

    return res;
}

static gboolean
_gst_cuda_memory_unmap(GstCudaMemory *mem)
{
    GST_DEBUG("Unmapped Memory: %p", mem);
    return TRUE;
}

static GstCudaMemory *
_gst_cuda_memory_share(GstCudaMemory *mem, gssize offset, gsize size)
{
    GST_WARNING("Implement!");
    return NULL;
}

static void
gst_cuda_allocator_class_init (GstCudaAllocatorClass * klass)
{
    GstAllocatorClass *allocator_class;

    allocator_class = GST_ALLOCATOR_CLASS(klass);

    allocator_class->alloc = _gst_cuda_allocator_alloc;
    allocator_class->free = _gst_cuda_allocator_free;
}

static void
gst_cuda_allocator_init(GstCudaAllocator * allocator)
{
    GstAllocator *alloc;

    alloc = GST_ALLOCATOR_CAST (allocator);

    alloc->mem_type = GST_CUDA_MEMORY_TYPE;
    alloc->mem_map = (GstMemoryMapFunction)_gst_cuda_memory_map;
    alloc->mem_unmap = (GstMemoryUnmapFunction)_gst_cuda_memory_unmap;
    alloc->mem_share = (GstMemoryShareFunction)_gst_cuda_memory_share;
}

static gsize
gst_cuda_get_align_mask(void)
{
    long pagesize = 0;

    pagesize = sysconf(_SC_PAGESIZE);

    return pagesize - 1;
}

static gboolean
gst_cuda_get_align_params(GstVideoInfo *info, gsize *maxsize, gsize *align)
{
    gboolean ret = FALSE;
    long pagesize = 0;
    
    if(info && maxsize && align){
        pagesize = sysconf(_SC_PAGESIZE);
        *align = gst_cuda_get_align_mask();
        *maxsize = (GST_VIDEO_INFO_SIZE (info) + pagesize - 1) & ~(pagesize - 1);
    }else{
        GST_DEBUG("Bad parameters!");
    }

    return ret;
}

GstAllocator *
gst_cuda_allocator_get(void)
{
    return gst_allocator_find(GST_CUDA_MEMORY_TYPE);
}

GstAllocationParams 
gst_cuda_allocator_get_params(void)
{
    GstAllocationParams params;

    gst_allocation_params_init(&params);
    params.align = gst_cuda_get_align_mask();

    return params;
}

GstMemory*
gst_cuda_allocator_alloc(GstVideoInfo *info)
{
    GstCudaMemory *mem = NULL;
    gsize maxsize = 0;
    gsize align = 0;

    gst_cuda_get_align_params(info, &maxsize, &align);

    GST_DEBUG("Allocate size: %"G_GSIZE_FORMAT, GST_VIDEO_INFO_SIZE (info));
    GST_DEBUG("      maxsize: %"G_GSIZE_FORMAT, maxsize);
    GST_DEBUG("        align: %"G_GSIZE_FORMAT, align);

    mem = g_slice_new0(GstCudaMemory);
    gst_memory_init(GST_MEMORY_CAST(mem),
            GST_MEMORY_FLAG_NO_SHARE,
            _cuda_memory_allocator,
            NULL, // parent device
            maxsize,
            align, // alignment mask
            0, // offset
            GST_VIDEO_INFO_SIZE (info));

    return (GstMemory*)mem;
}

void
gst_cuda_memory_init(void)
{
    static volatile gsize init = 0;

    if(g_once_init_enter(&init)){
        _cuda_memory_allocator = g_object_new(gst_cuda_allocator_get_type(), NULL);
        gst_object_ref_sink(_cuda_memory_allocator);
        gst_allocator_register(GST_CUDA_MEMORY_TYPE, gst_object_ref(_cuda_memory_allocator));
        g_once_init_leave(&init, 1);
    }
}
