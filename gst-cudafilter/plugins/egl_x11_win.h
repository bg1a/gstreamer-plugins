#ifndef __GST_CUDA_EGL_X11_WIN__
#define __GST_CUDA_EGL_X11_WIN__

#include <gst/gst.h>

/*
 * See:
 * iMX6 Gstreamer EGL-X11 window
 * https://github.com/Freescale/gstreamer-imx/blob/master/src/eglvivsink/egl_platform_x11.c
 * Simple EGL window:
 * NVIDIA_CUDA-9.0_Samples/.../graphics_interface.c
 */

G_BEGIN_DECLS

typedef void (*GstCudaEglX11WinRenderFn)(void *data);

typedef struct GstCudaEglX11WinRenderCallback
{
    GstCudaEglX11WinRenderFn render_fn;
    void *data;
}GstCudaEglX11WinRenderCallback;

inline void 
gst_egl_x11_win_init_cb(GstCudaEglX11WinRenderCallback *cb,
        GstCudaEglX11WinRenderFn fn, void *data)
{
    if(cb){
        cb->render_fn = fn;
        cb->data = data;
    }
}

inline void
gst_egl_x11_win_copy_cb(GstCudaEglX11WinRenderCallback *dst_cb,
        const GstCudaEglX11WinRenderCallback *src_cb)
{
    if(dst_cb && src_cb)
    {
        dst_cb->render_fn = src_cb->render_fn;
        dst_cb->data = src_cb->data;
    }
}

inline void
gst_egl_x11_win_run_cb(GstCudaEglX11WinRenderCallback *cb)
{
    if(cb && cb->render_fn){
        cb->render_fn(cb->data);
    }
}


typedef struct _GstCudaEglX11Win GstCudaEglX11Win;

GstCudaEglX11Win *gst_egl_x11_win_create(void);
void gst_egl_x11_win_destroy(GstCudaEglX11Win *wHandle);
void gst_egl_x11_win_close(GstCudaEglX11Win *wHandle);
gboolean gst_egl_x11_win_display(GstCudaEglX11Win *wHandle, guintptr parent_win,
        GstCudaEglX11WinRenderCallback *render_init_cb,
        GstCudaEglX11WinRenderCallback *render_cb,
        GstCudaEglX11WinRenderCallback *render_close_cb,
        int xpos, int ypos, int width, int height);


G_END_DECLS
#endif /* __GST_CUDA_EGL_X11_WIN__ */
