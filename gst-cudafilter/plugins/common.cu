#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "common.cuh"

static inline cudaError_t
checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
    if(result != cudaSuccess){
        fprintf(stderr, "CUDA Runtime Error: %s\n",
                cudaGetErrorString(result));
        assert(result == cudaSuccess);
    }
#endif
    return result;
}

extern "C" unsigned char *gst_cuda_alloc(unsigned int size)
{
    unsigned char *ptr = NULL;

    checkCuda(cudaMallocManaged(&ptr, size, cudaMemAttachGlobal));
   
    return ptr;
}

extern "C" unsigned char *gst_cuda_alloc_device(unsigned int size)
{
    unsigned char *ptr = NULL;

    checkCuda(cudaMalloc(&ptr, size));

    return ptr;
}

extern "C" void gst_cuda_free(unsigned char *ptr)
{
    cudaFree(ptr);
}


extern "C" void 
cuda_copy_host_to_device(unsigned char *dest_ptr, const unsigned char *src_ptr, unsigned int size)
{
    if(dest_ptr && src_ptr && size){
        checkCuda(cudaMemcpy(dest_ptr, src_ptr, size, cudaMemcpyHostToDevice));
    }
}

extern "C" void
cuda_copy_device_to_device(unsigned char *dest_ptr, const unsigned char *src_ptr, unsigned int size)
{
    if(dest_ptr && src_ptr && size){
        checkCuda(cudaMemcpy(dest_ptr, src_ptr, size, cudaMemcpyDeviceToDevice));
    }
}

__global__ void
cuda_bgra_to_rgba_kernel(unsigned char *dst, unsigned char *src, 
        int width, int height)
{
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if((row < height) && (col < width)){
        int byte_offset = row * width * 4 + col * 4;
        
        dst[byte_offset]        = src[byte_offset + 2];
        dst[byte_offset + 1]    = src[byte_offset + 1];
        dst[byte_offset + 2]    = src[byte_offset];
        dst[byte_offset + 3]    = src[byte_offset + 3];
    }
}

extern "C" int
cuda_bgra_to_rgba(unsigned char *dev_dst, unsigned char *dev_src, int width, int height)
{
    int ret = 0;

    if(dev_dst && dev_src && width && height){
        dim3 threads(32, 32);
        dim3 blocks( (threads.x + width - 1) / threads.x,
                     (threads.y + height - 1) / threads.y );

        cuda_bgra_to_rgba_kernel<<<blocks,threads>>>(dev_dst, dev_src, width, height);
        cudaStreamSynchronize(NULL);
    }else{
        fprintf(stderr, "Bad arguments in %s\n", __func__);    
        ret = -1;
    }

    return ret;
}

