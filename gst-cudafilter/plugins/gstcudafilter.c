/* GStreamer
 * Copyright (C) 2019 bg1a <bg1a@yandex.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstcudafilter
 *
 * The cudafilter element does transform/convert an input frame using CUDA API.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! cudafilter ! fakesink
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstcudafilter.h"

#include "filter.cuh"
#include "allocator.h"
#include "bufferpool.h"

GST_DEBUG_CATEGORY_STATIC (gst_cudafilter_debug_category);
#define GST_CAT_DEFAULT gst_cudafilter_debug_category

/* prototypes */
static void gst_cudafilter_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_cudafilter_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_cudafilter_dispose (GObject * object);
static void gst_cudafilter_finalize (GObject * object);

static gboolean gst_cudafilter_start (GstBaseTransform * trans);
static gboolean gst_cudafilter_stop (GstBaseTransform * trans);
static gboolean gst_cudafilter_set_info (GstVideoFilter * filter,
    GstCaps * incaps, GstVideoInfo * in_info, GstCaps * outcaps,
    GstVideoInfo * out_info);
static GstFlowReturn gst_cudafilter_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_cudafilter_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);
static gboolean gst_cudafilter_propose_allocation(GstBaseTransform *trans, 
        GstQuery *decide_query, GstQuery *query);
static gboolean gst_cudafilter_decide_allocation(GstBaseTransform *trans,
        GstQuery *query);

enum
{
  PROP_0
};

/* pad templates */
#define VIDEO_SRC_CAPS \
    GST_VIDEO_CAPS_MAKE("{ BGRx }")

#define VIDEO_SINK_CAPS \
    GST_VIDEO_CAPS_MAKE("{ BGRx }")

/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstCudafilter, gst_cudafilter, GST_TYPE_VIDEO_FILTER,
    GST_DEBUG_CATEGORY_INIT (gst_cudafilter_debug_category, "cudafilter", 0,
        "debug category for cudafilter element"));

static void
gst_cudafilter_class_init (GstCudafilterClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class =
      GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS (klass),
      gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS,
          gst_caps_from_string (VIDEO_SRC_CAPS)));
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS (klass),
      gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
          gst_caps_from_string (VIDEO_SINK_CAPS)));

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS (klass),
      "CUDA transform element", "Generic", "CUDA converter/filter",
      "bg1a <bg1a@yandex.ru>");

  gobject_class->set_property = gst_cudafilter_set_property;
  gobject_class->get_property = gst_cudafilter_get_property;
  gobject_class->dispose = gst_cudafilter_dispose;
  gobject_class->finalize = gst_cudafilter_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_cudafilter_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_cudafilter_stop);
  base_transform_class->propose_allocation = gst_cudafilter_propose_allocation;
  base_transform_class->decide_allocation = gst_cudafilter_decide_allocation;

  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_cudafilter_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_cudafilter_transform_frame);
  video_filter_class->transform_frame_ip =
      GST_DEBUG_FUNCPTR (gst_cudafilter_transform_frame_ip);

}

static void
gst_cudafilter_init (GstCudafilter * cudafilter)
{
}

static GstBufferPool*
gst_cudafilter_create_pool(GstBaseTransform *self, GstCaps *caps, GstVideoInfo *info)
{
    GstBufferPool *pool = NULL;
    GstStructure *config = NULL;
    const guint min_buffers = GST_CUDA_BUFFERPOOL_MIN_BUFFERS;
    const guint max_buffers = GST_CUDA_BUFFERPOOL_MAX_BUFFERS; 

    if(self && caps && info){
        if( (pool = gst_cuda_bufferpool_new()) ){
            GstAllocator *allocator = gst_cuda_allocator_get();
            GstAllocationParams params = gst_cuda_allocator_get_params();
            config = gst_buffer_pool_get_config(pool);

            gst_buffer_pool_config_set_params(config, caps, GST_VIDEO_INFO_SIZE(info),
                    min_buffers, max_buffers);
            gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_VIDEO_META);
            gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META);
            gst_buffer_pool_config_set_allocator(config, allocator, &params);

            if(!gst_buffer_pool_set_config(pool, config)){
                GST_WARNING_OBJECT(self, "Failed to set a buffer pool config!");
                gst_object_unref(pool);
                pool = NULL;
            }
            gst_object_unref(allocator);
        }else{
            GST_WARNING_OBJECT(self, "Failed to allocate buffer pool!");
        }
    }else{
        GST_WARNING_OBJECT(self, "Bad parameters!");
    }

    return pool;
}

static gboolean 
gst_cudafilter_propose_allocation(GstBaseTransform *trans, GstQuery *decide_query, GstQuery *query)
{
    gboolean need_pool = FALSE;
    GstCaps *caps = NULL;
    GstVideoInfo info;
    gboolean ret = FALSE;
    GstBufferPool *pool = NULL;
    gsize size = 0;

    GST_DEBUG_OBJECT(trans, "Propose allocation");

    gst_video_info_init(&info);
    gst_query_parse_allocation(query, &caps, &need_pool);
    if(caps && gst_video_info_from_caps(&info, caps)){
        
        size = GST_VIDEO_INFO_SIZE(&info);
        GST_DEBUG_OBJECT(trans, "Pool size: %"G_GSIZE_FORMAT, size);

        if(need_pool && (pool = gst_cudafilter_create_pool(trans, caps, &info))){
            gst_query_add_allocation_pool(query, pool, size, 
                    GST_CUDA_BUFFERPOOL_MIN_BUFFERS, GST_CUDA_BUFFERPOOL_MAX_BUFFERS);
            gst_object_unref(pool);

            ret = TRUE;
        }else{
            GST_WARNING_OBJECT(trans, "Could not allocate pool (need pool %d)!", need_pool);
        }
    }else{
        GST_DEBUG_OBJECT(trans, "Failed to parse allocation!");    
    }

    return ret;
}

static gboolean 
gst_cudafilter_decide_allocation(GstBaseTransform *trans, GstQuery *query)
{
    GstCaps *caps = NULL;
    GstVideoInfo info;
    GstBufferPool *pool = NULL;
    gboolean ret = TRUE;
    gboolean update_pool = FALSE;
    guint size = 0;
    guint min = 0;
    guint max = 0;
    
    GST_DEBUG_OBJECT(trans, "Decide allocation");

    gst_video_info_init(&info);
    gst_query_parse_allocation(query, &caps, NULL);
    if(caps && gst_video_info_from_caps(&info, caps)){
        GstStructure *config = NULL;

        if(gst_query_get_n_allocation_pools (query) > 0){
            guint tmp_size, tmp_min, tmp_max;

            gst_query_parse_nth_allocation_pool(query, 0, &pool, &tmp_size, &tmp_min, &tmp_max);
            
            if(pool){
                if(!gst_buffer_pool_has_option(pool, GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META)){
                    GST_DEBUG_OBJECT(trans, "Current buffer pool is not %s",
                            GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META);
                    gst_object_unref(pool); 
                    pool = NULL;
                }else{
                    GST_DEBUG_OBJECT(trans, "Need to update a Cuda buffer pool");
                    update_pool = TRUE;
                }
            }

            size = MAX(tmp_size, GST_VIDEO_INFO_SIZE(&info));
            min = tmp_min;
            max = tmp_max;
        }else{
            size = GST_VIDEO_INFO_SIZE(&info);
            min = GST_CUDA_BUFFERPOOL_MIN_BUFFERS;
            max = GST_CUDA_BUFFERPOOL_MAX_BUFFERS;
        }

        if(!pool){
            pool = gst_cudafilter_create_pool(trans, caps, &info);  
        }

        config = gst_buffer_pool_get_config(pool);
        gst_buffer_pool_config_set_params(config, caps, size, min, max);
        gst_buffer_pool_set_config(pool, config);

        if(update_pool){
            gst_query_set_nth_allocation_pool(query, 0, pool, size, min, max); 
        }else{
            gst_query_add_allocation_pool(query, pool, size, min, max); 
        }
        gst_object_unref(pool);
    }else{
        GST_ERROR_OBJECT(trans, "Failed to parse caps!");
    }

    return ret;
}

void
gst_cudafilter_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (object);

  GST_DEBUG_OBJECT (cudafilter, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cudafilter_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (object);

  GST_DEBUG_OBJECT (cudafilter, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cudafilter_dispose (GObject * object)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (object);

  GST_DEBUG_OBJECT (cudafilter, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_cudafilter_parent_class)->dispose (object);
}

void
gst_cudafilter_finalize (GObject * object)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (object);

  GST_DEBUG_OBJECT (cudafilter, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_cudafilter_parent_class)->finalize (object);
}

static gboolean
gst_cudafilter_start (GstBaseTransform * trans)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (trans);

  GST_DEBUG_OBJECT (cudafilter, "start");

  return TRUE;
}

static gboolean
gst_cudafilter_stop (GstBaseTransform * trans)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (trans);

  GST_DEBUG_OBJECT (cudafilter, "stop");

  return TRUE;
}

static gboolean
gst_cudafilter_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (filter);
  GstStructure *structure;

  GST_DEBUG_OBJECT (cudafilter, "set_info");

  structure = gst_caps_get_structure(incaps, 0);

  GST_OBJECT_LOCK(cudafilter);
  gst_structure_get_int(structure, "width", &cudafilter->width);
  gst_structure_get_int(structure, "height", &cudafilter->height);
  GST_OBJECT_UNLOCK(cudafilter);
  
  GST_DEBUG_OBJECT(cudafilter, "width %d height %d", cudafilter->width, cudafilter->height);

  return TRUE;
}

/* transform */
static GstFlowReturn
gst_cudafilter_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (filter);
  gint width = cudafilter->width;
  gint height = cudafilter->height;
  GstMemory *inmem = NULL;
  GstMemory *outmem = NULL;
  GstBuffer *inbuf = inframe->buffer;
  GstBuffer *outbuf = outframe->buffer;
  GstFlowReturn ret = GST_FLOW_OK;

  GST_DEBUG_OBJECT (cudafilter, "-----> transform_frame");

  inmem = gst_buffer_peek_memory(inbuf, 0);
  outmem = gst_buffer_peek_memory(outbuf, 0);

  if(gst_memory_is_type(inmem, GST_CUDA_MEMORY_TYPE) && gst_memory_is_type(outmem, GST_CUDA_MEMORY_TYPE)){
       filter_copy_bpp32(width, height, 
              GST_VIDEO_FRAME_PLANE_DATA(outframe, 0), GST_VIDEO_FRAME_PLANE_DATA(inframe, 0));
      /* Just to compare different types of memory copying
       * cuda_copy_device_to_device(GST_VIDEO_FRAME_PLANE_DATA(outframe, 0),
       *       GST_VIDEO_FRAME_PLANE_DATA(inframe, 0), GST_VIDEO_FRAME_SIZE(outframe));
       * gst_video_frame_copy(outframe, inframe);
       * memcpy(GST_VIDEO_FRAME_PLANE_DATA(outframe, 0), GST_VIDEO_FRAME_PLANE_DATA(inframe, 0),
       *         width * height * 4);
       */
  }else{
    GST_ERROR_OBJECT(cudafilter, "Failed to copy videoframe, buffers not allocated by Cuda allocator!");
    GST_ERROR_OBJECT(cudafilter, "  inframe allocated by Cuda? %d", 
            gst_memory_is_type(inmem, GST_CUDA_MEMORY_TYPE));
    GST_ERROR_OBJECT(cudafilter, "  outframe allocated by Cuda? %d", 
            gst_memory_is_type(outmem,GST_CUDA_MEMORY_TYPE));
    ret = GST_FLOW_ERROR;
  }
  GST_DEBUG_OBJECT (cudafilter, "<----- transform_frame");

  return ret;
}

static GstFlowReturn
gst_cudafilter_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame)
{
  GstCudafilter *cudafilter = GST_CUDAFILTER (filter);

  GST_DEBUG_OBJECT (cudafilter, "transform_frame_ip");

  return GST_FLOW_OK;
}
