#ifndef __GST_CUDA_EGL_GLES__
#define __GST_CUDA_EGL_GLES__

#include <gst/gst.h>
#include <gst/video/video.h>

/*
 * See:
 * iMX6 Gstreamer EGL-X11 window
 * https://github.com/Freescale/gstreamer-imx/blob/master/src/eglvivsink/gles2_renderer.c
 * Simple EGL window:
 * NVIDIA_CUDA-9.0_Samples/.../graphics_interface.c
 */

G_BEGIN_DECLS

typedef struct _GstCudaEglGles GstCudaEglGles;

GstCudaEglGles *gst_egl_gles_create(void);
void gst_egl_gles_destroy(GstCudaEglGles *egles);
gboolean gst_egl_gles_start(GstCudaEglGles *egles, const GstVideoInfo *info, 
        guintptr parent_win, int xpos, int ypos);
void gst_egl_gles_stop(GstCudaEglGles *egles);
gboolean gst_egl_gles_display_frame(GstCudaEglGles *egles, GstBuffer *buf);

G_END_DECLS
#endif /* __GST_CUDA_EGL_GLES__ */
