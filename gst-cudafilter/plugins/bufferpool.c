#include <gst/video/gstvideometa.h>

#include "allocator.h"
#include "bufferpool.h"

/*
 * See:
 * gst-plugins-bad-1.14.4/sys/vdpau/gstvdpvideobufferpool.c
 * gst-plugins-bad-1.14.4/sys/kms/gstkmsbufferpool.c
 */


#define GST_CAT_DEFAULT GST_CAT_CUDA_BUFFERPOOL
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);

#define parent_class gst_cuda_bufferpool_parent_class
G_DEFINE_TYPE_WITH_CODE(GstCudaBufferpool, gst_cuda_bufferpool, GST_TYPE_BUFFER_POOL,
        GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "cudabufferpool", 0,
            "Cuda Buffer Pool"));

static const gchar **
gst_cuda_bufferpool_get_options(GstBufferPool *pool)
{
    static const gchar *options[] = { GST_BUFFER_POOL_OPTION_VIDEO_META,
        GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META, NULL };

    return options;
}

static gboolean
gst_cuda_bufferpool_set_config(GstBufferPool *pool, GstStructure *config)
{
    GstCudaBufferpool *cpool = GST_CUDA_BUFFERPOOL(pool);
    GstVideoInfo info;
    GstCaps *caps;
    gboolean ret = FALSE;

    if(gst_buffer_pool_config_get_params(config, &caps, NULL, NULL, NULL) &&
            gst_video_info_from_caps(&info, caps)){

        GST_LOG_OBJECT(pool, "Caps %"GST_PTR_FORMAT", image size %dx%d", caps, 
                info.width, info.height); 

        if(GST_VIDEO_INFO_FORMAT(&info) != GST_VIDEO_FORMAT_UNKNOWN){
            cpool->info = info;

            cpool->add_videometa = gst_buffer_pool_config_has_option(config, 
                    GST_BUFFER_POOL_OPTION_VIDEO_META);

            ret = GST_BUFFER_POOL_CLASS(parent_class)->set_config(pool, config);
        }else{
            GST_WARNING_OBJECT(pool, "Video format unknown!");
        }
    }else{
        GST_WARNING_OBJECT(pool, "Wrong config");
    }

    return ret;
}

static GstFlowReturn
gst_cuda_bufferpool_alloc_buffer(GstBufferPool *pool, GstBuffer **buffer,
        GstBufferPoolAcquireParams * params)
{
    GstCudaBufferpool *cpool = NULL;
    GstBuffer *buf = NULL;
    GstVideoInfo *info = NULL;
    GstFlowReturn ret = GST_FLOW_ERROR;
    GstMemory *mem = NULL;

    cpool = GST_CUDA_BUFFERPOOL(pool);
    info = &cpool->info;

    if( (buf = gst_buffer_new()) ){
        if( (mem = gst_cuda_allocator_alloc(info)) ){
            gst_buffer_append_memory(buf, mem);

            if(cpool->add_videometa){
                GST_DEBUG_OBJECT(pool, "Adding meta ...");
                gst_buffer_add_video_meta_full(buf, GST_VIDEO_FRAME_FLAG_NONE,
                        GST_VIDEO_INFO_FORMAT(info), 
                        GST_VIDEO_INFO_WIDTH(info), GST_VIDEO_INFO_HEIGHT(info),
                        GST_VIDEO_INFO_N_PLANES(info), info->offset, info->stride);
            }

            *buffer = buf;
            ret = GST_FLOW_OK;
        }else{
            GST_WARNING_OBJECT(pool, "Failed to allocate memory!");
        } 
    }else{
        GST_WARNING_OBJECT(pool, "Failed to allocate buffer!");
    }

    return ret;
}

static void
gst_cuda_bufferpool_init(GstCudaBufferpool *pool)
{
}

static void
gst_cuda_bufferpool_finalize(GObject *object)
{
   GstCudaBufferpool *pool = GST_CUDA_BUFFERPOOL(object);

   GST_LOG_OBJECT(pool, "Finalizing Object: %p", pool);
   G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void
gst_cuda_bufferpool_class_init(GstCudaBufferpoolClass *klass)
{
    GObjectClass *gobject_class = (GObjectClass *)klass;
    GstBufferPoolClass *gstbufferpool_class = (GstBufferPoolClass *)klass;

    gobject_class->finalize = gst_cuda_bufferpool_finalize;

    gstbufferpool_class->get_options = gst_cuda_bufferpool_get_options;
    gstbufferpool_class->set_config = gst_cuda_bufferpool_set_config;
    gstbufferpool_class->alloc_buffer = gst_cuda_bufferpool_alloc_buffer;
}

GstBufferPool *gst_cuda_bufferpool_new(void)
{
    GstBufferPool *pool = NULL;

    pool = g_object_new(GST_TYPE_CUDA_BUFFERPOOL, NULL);
    gst_object_ref_sink(pool);

    GST_DEBUG_OBJECT(pool, "New buffer pool: %p", pool);

    return GST_BUFFER_POOL_CAST(pool);
}
