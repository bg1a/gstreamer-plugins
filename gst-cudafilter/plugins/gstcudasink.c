/* GStreamer
 * Copyright (C) 2019 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstcudasink
 *
 * The cudasink element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! cudasink ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideosink.h>

#include "common.cuh"
#include "gstcudasink.h"
#include "egl_gles.h"

#include "bufferpool.h"

GST_DEBUG_CATEGORY_STATIC (gst_cudasink_debug_category);
#define GST_CAT_DEFAULT gst_cudasink_debug_category

#define GST_CUDA_BUFFERPOOL_MIN_BUFFERS 2
#define GST_CUDA_BUFFERPOOL_MAX_BUFFERS 0 // 0 - unlimited
/* prototypes */


static void gst_cudasink_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_cudasink_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_cudasink_dispose (GObject * object);
static void gst_cudasink_finalize (GObject * object);
static GstFlowReturn gst_cudasink_show_frame (GstVideoSink * video_sink,
    GstBuffer * buf);

static gboolean gst_cuda_sink_set_caps(GstBaseSink *sink, GstCaps *caps);
static gboolean gst_cudasink_propose_allocation(GstBaseSink *sink, GstQuery *query);

enum
{
  PROP_0
};

/* pad templates */

#define VIDEO_SINK_CAPS \
    GST_VIDEO_CAPS_MAKE("{ BGRx }")


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstCudasink, gst_cudasink, GST_TYPE_VIDEO_SINK,
  GST_DEBUG_CATEGORY_INIT (gst_cudasink_debug_category, "cudasink", 0,
  "debug category for cudasink element"));

static void
gst_cudasink_class_init (GstCudasinkClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstVideoSinkClass *video_sink_class = GST_VIDEO_SINK_CLASS (klass);
  GstBaseSinkClass *base_class = GST_BASE_SINK_CLASS(klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
      gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        gst_caps_from_string (VIDEO_SINK_CAPS)));

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "FIXME Long name", "Generic", "FIXME Description",
      "FIXME <fixme@example.com>");

  gobject_class->set_property = gst_cudasink_set_property;
  gobject_class->get_property = gst_cudasink_get_property;
  gobject_class->dispose = gst_cudasink_dispose;
  gobject_class->finalize = gst_cudasink_finalize;
  video_sink_class->show_frame = GST_DEBUG_FUNCPTR (gst_cudasink_show_frame);
  base_class->set_caps = GST_DEBUG_FUNCPTR(gst_cuda_sink_set_caps);
  base_class->propose_allocation = GST_DEBUG_FUNCPTR(gst_cudasink_propose_allocation);
}

static void
gst_cudasink_init (GstCudasink *cudasink)
{
    if(cudasink){
        cudasink->egles = gst_egl_gles_create();
        cudasink->egles_started = 0;
        cudasink->current_buf = NULL;
    }
}

void
gst_cudasink_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstCudasink *cudasink = GST_CUDASINK (object);

  GST_DEBUG_OBJECT (cudasink, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cudasink_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstCudasink *cudasink = GST_CUDASINK (object);

  GST_DEBUG_OBJECT (cudasink, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_cudasink_dispose (GObject * object)
{
  GstCudasink *cudasink = GST_CUDASINK (object);

  GST_DEBUG_OBJECT (cudasink, "dispose");

  G_OBJECT_CLASS (gst_cudasink_parent_class)->dispose (object);
}

void
gst_cudasink_finalize (GObject * object)
{
  GstCudasink *cudasink = GST_CUDASINK (object);

  GST_DEBUG_OBJECT (cudasink, "finalize");
  
  GST_OBJECT_LOCK(cudasink);
  if(cudasink->egles){
    if(cudasink->egles_started){
      gst_egl_gles_stop(cudasink->egles);
      cudasink->egles_started = 0;
    }
    gst_egl_gles_destroy(cudasink->egles);
    cudasink->egles = NULL;
  }
  GST_OBJECT_UNLOCK(cudasink);

  G_OBJECT_CLASS (gst_cudasink_parent_class)->finalize (object);
}

static GstBufferPool*
gst_cudasink_create_pool(GstBaseSink *self, GstCaps *caps, GstVideoInfo *info)
{
    GstBufferPool *pool = NULL;
    GstStructure *config = NULL;
    const guint min_buffers = GST_CUDA_BUFFERPOOL_MIN_BUFFERS;
    const guint max_buffers = GST_CUDA_BUFFERPOOL_MAX_BUFFERS; 

    if(self && caps && info){
        if( (pool = gst_cuda_bufferpool_new()) ){
            GstAllocator *allocator = gst_cuda_allocator_get();
            GstAllocationParams params = gst_cuda_allocator_get_params();
            config = gst_buffer_pool_get_config(pool);

            gst_buffer_pool_config_set_params(config, caps, GST_VIDEO_INFO_SIZE(info),
                    min_buffers, max_buffers);
            gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_VIDEO_META);
            gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META);
            gst_buffer_pool_config_set_allocator(config, allocator, &params);

            if(!gst_buffer_pool_set_config(pool, config)){
                GST_WARNING_OBJECT(self, "Failed to set a buffer pool config!");
                gst_object_unref(pool);
                pool = NULL;
            }
            gst_object_unref(allocator);
        }else{
            GST_WARNING_OBJECT(self, "Failed to allocate buffer pool!");
        }
    }else{
        GST_WARNING_OBJECT(self, "Bad parameters!");
    }

    return pool;
}

static gboolean 
gst_cudasink_propose_allocation(GstBaseSink *sink, GstQuery *query)
{
    gboolean need_pool = FALSE;
    GstCaps *caps = NULL;
    GstVideoInfo info;
    gboolean ret = FALSE;
    GstBufferPool *pool = NULL;
    gsize size = 0;

    GST_DEBUG_OBJECT(sink, "Propose allocation");

    gst_video_info_init(&info);
    gst_query_parse_allocation(query, &caps, &need_pool);
    if(caps && gst_video_info_from_caps(&info, caps)){
        
        size = GST_VIDEO_INFO_SIZE(&info);
        GST_DEBUG_OBJECT(sink, "Pool size: %"G_GSIZE_FORMAT, size);

        if(need_pool && (pool = gst_cudasink_create_pool(sink, caps, &info))){
            gst_query_add_allocation_pool(query, pool, size, 
                    GST_CUDA_BUFFERPOOL_MIN_BUFFERS, GST_CUDA_BUFFERPOOL_MAX_BUFFERS);
            gst_object_unref(pool);

            ret = TRUE;
        }else{
            GST_WARNING_OBJECT(sink, "Could not allocate pool (need pool %d)!", need_pool);
        }
    }else{
        GST_DEBUG_OBJECT(sink, "Failed to parse allocation!");    
    }

    return ret;
}

static gboolean 
gst_cuda_sink_set_caps(GstBaseSink *sink, GstCaps *caps)
{
  GstCudasink *cudasink = GST_CUDASINK (sink);
  gboolean ret = TRUE; 

  GST_DEBUG_OBJECT (cudasink, "set_caps");

  GST_OBJECT_LOCK(cudasink);
  if(cudasink->egles_started){
    gst_egl_gles_stop(cudasink->egles);
    cudasink->egles_started = 0;
  }

  if(gst_video_info_from_caps(&cudasink->info, caps)){
    ret = gst_egl_gles_start(cudasink->egles, &cudasink->info, 0, 0, 0);
    if(ret){
      GST_DEBUG_OBJECT(cudasink, "EGL GLES sink started");
      cudasink->egles_started = TRUE;
    }
  }else{
    GST_DEBUG_OBJECT(cudasink, "Failed to get video info!");
    ret = FALSE;
  }
  GST_OBJECT_UNLOCK(cudasink);

  return TRUE;
}

static GstFlowReturn
gst_cudasink_show_frame (GstVideoSink * sink, GstBuffer * buf)
{
  GstCudasink *cudasink = GST_CUDASINK (sink);
  GstMemory *mem = NULL;
  GstFlowReturn ret = GST_FLOW_OK;

  GST_DEBUG_OBJECT (cudasink, "show_frame %p", buf);

  if(cudasink->egles_started){

      mem = gst_buffer_peek_memory(buf, 0);
      if(gst_memory_is_type(mem, GST_CUDA_MEMORY_TYPE)){
          GST_INFO("Buffer was allocated by %s", GST_CUDA_MEMORY_TYPE);
          gst_egl_gles_display_frame(cudasink->egles, buf);
      }else{
          GST_ERROR("Buffer was not allocated by %s!", GST_CUDA_MEMORY_TYPE);
      }
  }else{
      GST_ERROR("Can't display a frame, EGL-GLES has not started yet!");
      ret = GST_FLOW_ERROR;
  }

  return ret;
}
