#ifndef __CUDA_COMMON_H__
#define __CUDA_COMMON_H__

#define DEBUG 1

#ifdef __cplusplus
extern "C"{
#endif

unsigned char *gst_cuda_alloc(unsigned int size);
unsigned char *gst_cuda_alloc_device(unsigned int size);
void gst_cuda_free(unsigned char *ptr);

void cuda_copy_host_to_device(unsigned char *dest_ptr, 
        const unsigned char *src_ptr, unsigned int size);

void cuda_copy_device_to_device(unsigned char *dest_ptr, 
        const unsigned char *src_ptr, unsigned int size);

int cuda_bgra_to_rgba(unsigned char *dev_dst, unsigned char *dev_src, int width, int height);
#ifdef __cplusplus
}
#endif

#endif /* __CUDA_COMMON_H__ */
