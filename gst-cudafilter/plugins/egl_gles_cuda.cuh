#ifndef __EGL_GLES_CUH__
#define __EGL_GLES_CUH__

#ifdef __cplusplus
extern "C"{
#endif

#include <GLES3/gl31.h>
#include <GL/gl.h>
#include <GL/glext.h>

typedef struct _EglGlesCudaContext EglGlesCudaContext;

EglGlesCudaContext *egl_gles_cuda_context_create(void);
void egl_gles_cuda_context_destroy(EglGlesCudaContext *egl_cuda_ctx);

/* Register OpenGL texture with a Cuda API
 * Returns: 0 - on success, otherwise failure
 */
int egl_gles_cuda_context_register_image(EglGlesCudaContext *egl_cuda_ctx, GLuint *tex);

/* Copy Cuda allocated data to the OpenGL texture
 * Returns: 0 - on success, otherwise failure
 */
int egl_gles_cuda_context_copy_to_image(EglGlesCudaContext *egl_cuda_ctx,
        unsigned char *cuda_data, int cuda_data_size);

#ifdef __cplusplus
}
#endif

#endif /* __EGL_GLES_CUH__ */
