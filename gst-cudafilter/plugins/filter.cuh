#ifndef __CUDA_FILTER_H__
#define __CUDA_FILTER_H__

#define DEBUG 1

#ifdef __cplusplus
extern "C"{
#endif

void filter_copy_bpp32(int width, int height, unsigned char *dst, unsigned char *src);

#ifdef __cplusplus
}
#endif

#endif /* __CUDA_FILTER_H__ */
