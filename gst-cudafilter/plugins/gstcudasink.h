/* GStreamer
 * Copyright (C) 2019 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_CUDASINK_H_
#define _GST_CUDASINK_H_

#include <gst/video/video.h>
#include <gst/video/gstvideosink.h>

#include "allocator.h"
#include "egl_gles.h"

G_BEGIN_DECLS

#define GST_TYPE_CUDASINK   (gst_cudasink_get_type())
#define GST_CUDASINK(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_CUDASINK,GstCudasink))
#define GST_CUDASINK_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_CUDASINK,GstCudasinkClass))
#define GST_IS_CUDASINK(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_CUDASINK))
#define GST_IS_CUDASINK_CLASS(obj)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_CUDASINK))

typedef struct _GstCudasink GstCudasink;
typedef struct _GstCudasinkClass GstCudasinkClass;

struct _GstCudasink
{
  GstVideoSink base_cudasink;
  
  GstVideoInfo info;
  GstBuffer *current_buf;
  GstCudaEglGles *egles;
  gint egles_started;
};

struct _GstCudasinkClass
{
  GstVideoSinkClass base_cudasink_class;
};

GType gst_cudasink_get_type (void);

G_END_DECLS

#endif
