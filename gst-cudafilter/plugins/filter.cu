#include "filter.cuh"

__global__
void filter_copy_bpp32_kernel(int width, int height, unsigned char *dst, unsigned char *src)
{
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if(col < width && row < height){
        for(int k = 0; k < 4; ++k){
            dst[row * width * 4  + col * 4 + k] = src[row * width * 4 + col * 4 + k];
        }
    }
}

extern "C" void filter_copy_bpp32(int width, int height, unsigned char *dst, unsigned char *src)
{
    dim3 threadsPerBlock(32, 32);
    dim3 blocks((width + threadsPerBlock.x - 1) / threadsPerBlock.x,
                (height + threadsPerBlock.y - 1) / threadsPerBlock.y);

    filter_copy_bpp32_kernel<<<blocks, threadsPerBlock>>>(width, height, dst, src);
    cudaStreamSynchronize(NULL);
}

