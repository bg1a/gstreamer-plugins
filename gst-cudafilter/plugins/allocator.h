#ifndef __GST_CUDA_ALLOCATOR__
#define __GST_CUDA_ALLOCATOR__

#include <gst/gst.h>
#include <gst/gstmemory.h>
#include <gst/gstallocator.h>
#include <gst/video/video-info.h>

#define GST_CUDA_MEMORY_TYPE "CudaMemory"

G_BEGIN_DECLS

#define GST_TYPE_CUDA_ALLOCATOR \
    (gst_cuda_allocator_get_type())
#define GST_IS_CUDA_ALLOCATOR(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_CUDA_ALLOCATOR))
#define GST_IS_CUDA_ALLOCATOR_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_CUDA_ALLOCATOR))
#define GST_CUDA_ALLOCATOR_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_CUDA_ALLOCATOR, GstCudaAllocatorClass))
#define GST_CUDA_ALLOCATOR(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_CUDA_ALLOCATOR, GstCudaAllocator))
#define GST_CUDA_ALLOCATOR_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_CUDA_ALLOCATOR, GstCudaAllocatorClass))

typedef struct _GstCudaAllocator GstCudaAllocator;
typedef struct _GstCudaAllocatorClass GstCudaAllocatorClass;
typedef struct _GstCudaMemory GstCudaMemory; 

struct _GstCudaMemory
{
    GstMemory parent;
    
    unsigned char *data; // Allocate/free: cudaMallocManaged/cudaFree

    GstVideoInfo *info;
};

struct _GstCudaAllocator
{
    GstAllocator parent;
};

struct _GstCudaAllocatorClass
{
    GstAllocatorClass parent_class;
};

GType gst_cuda_allocator_get_type (void) G_GNUC_CONST;

gboolean gst_is_cuda_memory(GstMemory *mem);
void gst_cuda_memory_init(void);
GstMemory* gst_cuda_allocator_alloc(GstVideoInfo *info);

GstAllocator * gst_cuda_allocator_get(void);
GstAllocationParams gst_cuda_allocator_get_params(void);
G_END_DECLS

#endif /* __GST_CUDA_ALLOCATOR__ */
