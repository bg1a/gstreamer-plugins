#include "egl_x11_win.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

#define GST_EGL_X11_WIN_NAME "EGL Win"
#define GST_EGL_X11_THREAD_NAME "EGL Win Thread"

#define GST_CAT_DEFAULT GST_CAT_CUDA_EGL_X11_WIN
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);

static const EGLint configAttrs[] = {
    EGL_RED_SIZE,        1,
    EGL_GREEN_SIZE,      1,
    EGL_BLUE_SIZE,       1,
    EGL_DEPTH_SIZE,      16,
    EGL_SAMPLE_BUFFERS,  0,
    EGL_SAMPLES,         0,
    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    EGL_NONE
};
static const EGLint contextAttrs[] = {
    EGL_CONTEXT_CLIENT_VERSION, 3,
    EGL_NONE
};

typedef enum _egl_thread_state
{
    THREAD_STATE_NOT_STARTED = 0,
    THREAD_STATE_RUNNING,
    THREAD_STATE_STOPPED,
}egl_thread_state;

typedef enum _egl_thread_message
{
    THREAD_MESSAGE_NONE = 0,
    THREAD_MESSAGE_START,
    THREAD_MESSAGE_STOP,
}egl_thread_message;

struct _GstCudaEglX11Win{
    Display *display;
    int screen;
    Window win; 

    EGLDisplay eglDisplay;
    EGLSurface eglSurface;
    EGLContext eglContext;
    EGLConfig defaultEGLconfig;

    guintptr parent_win;
    int xpos;
    int ypos;
    int width;
    int height;

    GstCudaEglX11WinRenderCallback render_init_cb;
    GstCudaEglX11WinRenderCallback render_cb;
    GstCudaEglX11WinRenderCallback render_close_cb;

    GMutex mutex;

    GThread *thread;
    egl_thread_state thread_state;
    egl_thread_message thread_msg;
};


GstCudaEglX11Win *gst_egl_x11_win_create(void);
void gst_egl_x11_win_destroy(GstCudaEglX11Win *wHandle);

#define WHANDLE_MUTEX_LOCK(_wHandle) g_mutex_lock(&((_wHandle)->mutex))
#define WHANDLE_MUTEX_UNLOCK(_wHandle) g_mutex_unlock(&((_wHandle)->mutex))

static void
gst_egl_x11_win_init(void)
{
    static volatile gsize init = 0;

    if(g_once_init_enter(&init)){
        GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "cudaeglx11win", 0, "Cuda EGL X11");
        XInitThreads();
    }
}

static gboolean
create_win_context(GstCudaEglX11Win *wHandle)
{
    EGLConfig* configList = NULL;
    EGLDisplay eglDisplay = EGL_NO_DISPLAY;
    EGLint configCount;
    Display *display = NULL;
    int screen;
    gboolean ret = FALSE;

    display = XOpenDisplay(NULL);
    if(!display){
        GST_ERROR("Could not open X display!");
        return ret;
    }

    screen = DefaultScreen(display);
    eglDisplay = eglGetDisplay(display);
    if(EGL_NO_DISPLAY == eglDisplay){
        GST_ERROR("EGL failed to obtain display!");
        XCloseDisplay(display);
        return ret;
    }

    EGLint ver_major, ver_minor;
    if(eglInitialize(eglDisplay, &ver_major, &ver_minor)){
        GST_INFO("EGL major and minor: %d.%d", ver_major, ver_minor);
    }else{
        GST_ERROR("EGL failed to initialize!");
        XCloseDisplay(display);
        return ret;
    }

    if(!eglChooseConfig(eglDisplay, configAttrs, NULL, 0, &configCount) || !configCount){
        GST_ERROR("Failed to get EGL configuration!");
        XCloseDisplay(display);
        return ret;
    }

    configList = g_new0(EGLConfig, configCount);
    if (!eglChooseConfig(eglDisplay, configAttrs, configList, 
                configCount, &configCount) || !configCount){
        GST_ERROR("Failed to populate EGL configuration!");
        XCloseDisplay(display);
        return ret;
    }

    wHandle->display = display;
    wHandle->screen = screen;
    wHandle->defaultEGLconfig = configList[0];
    wHandle->eglDisplay = eglDisplay;

    g_free(configList);

    return TRUE;
}

static gboolean
create_win(GstCudaEglX11Win *wHandle)
{
    EGLint windowAttrs[] = {EGL_NONE};
    EGLSurface eglSurface = EGL_NO_SURFACE;
    EGLContext eglContext = EGL_NO_CONTEXT;
    long event_mask = ExposureMask;
    guintptr parent_win = 0;
    gboolean ret = FALSE;
    Window win = 0;

    if(wHandle->parent_win){
        parent_win = wHandle->parent_win;
    }else{
        parent_win = RootWindow(wHandle->display, wHandle->screen);
        event_mask |= StructureNotifyMask;
    }

    win = XCreateSimpleWindow(wHandle->display, parent_win,
                              wHandle->xpos, wHandle->ypos, wHandle->width, wHandle->height, 0,
                              BlackPixel(wHandle->display, wHandle->screen),
                              WhitePixel(wHandle->display, wHandle->screen));

    XStoreName(wHandle->display, win, GST_EGL_X11_WIN_NAME);
    XSelectInput(wHandle->display, win, event_mask);
    XMapWindow(wHandle->display, win);
    eglSurface = eglCreateWindowSurface(wHandle->eglDisplay, wHandle->defaultEGLconfig,
                                        (EGLNativeWindowType) win, windowAttrs);
    if(!eglSurface){
        GST_ERROR("EGL could not create window!");
        return ret;
    }

    eglBindAPI(EGL_OPENGL_ES_API);
    eglContext = eglCreateContext(wHandle->eglDisplay, wHandle->defaultEGLconfig, NULL, contextAttrs);
    if(!eglContext){
        GST_ERROR("EGL could not create a context!");
    }
    if (!eglMakeCurrent(wHandle->eglDisplay, eglSurface, eglSurface, eglContext)){
        GST_ERROR("EGL could not make context current!");
    }

    wHandle->win = win;
    wHandle->eglSurface = eglSurface;
    wHandle->eglContext = eglContext;

    return TRUE;
}

static void
destroy_win(GstCudaEglX11Win *wHandle)
{
    if(EGL_NO_DISPLAY != wHandle->eglDisplay){
        eglMakeCurrent(wHandle, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

        if(EGL_NO_CONTEXT != wHandle->eglContext){
            eglDestroyContext(wHandle->eglDisplay, wHandle->eglContext);
            wHandle->eglContext = EGL_NO_CONTEXT;
        }

        if(EGL_NO_SURFACE != wHandle->eglSurface){
            eglDestroySurface(wHandle->eglDisplay, wHandle->eglSurface);
            wHandle->eglSurface = EGL_NO_SURFACE;
        }
        wHandle->eglDisplay = EGL_NO_DISPLAY;
    }

    if(wHandle->win){
        XSelectInput(wHandle->display, wHandle->win, 0);
        while(XPending(wHandle->display)){
            XEvent xevent;
            XNextEvent(wHandle->display, &xevent);
        }

        XDestroyWindow(wHandle->display, wHandle->win);
        wHandle->win = 0;
    }
}

static gpointer 
window_mainloop(gpointer thread_data)
{
    GstCudaEglX11Win *wHandle = (GstCudaEglX11Win *)thread_data;

    if(wHandle){
        GstCudaEglX11WinRenderCallback render_cb;
        GstCudaEglX11WinRenderCallback render_init_cb;
        GstCudaEglX11WinRenderCallback render_close_cb;
        Display *display;
        EGLDisplay eglDisplay;
        EGLSurface eglSurface;
        gboolean ret = FALSE;
        XEvent event;		

        g_atomic_int_set(&wHandle->thread_state, THREAD_STATE_RUNNING);

        GST_INFO("Entering mainloop ...");
        WHANDLE_MUTEX_LOCK(wHandle);
        ret = create_win(wHandle);
        WHANDLE_MUTEX_UNLOCK(wHandle);

        if(ret){
            WHANDLE_MUTEX_LOCK(wHandle);
            display = wHandle->display;
            eglDisplay = wHandle->eglDisplay;
            eglSurface = wHandle->eglSurface;

            gst_egl_x11_win_copy_cb(&render_cb, &wHandle->render_cb);
            gst_egl_x11_win_copy_cb(&render_init_cb, &wHandle->render_init_cb);
            gst_egl_x11_win_copy_cb(&render_close_cb, &wHandle->render_close_cb);
            WHANDLE_MUTEX_UNLOCK(wHandle);

            gst_egl_x11_win_run_cb(&render_init_cb);
            while(1){
                if(THREAD_MESSAGE_STOP == g_atomic_int_get(&wHandle->thread_msg)) break;

                if(XPending(display)){
                    XNextEvent(display, &event);
                    /* TODO: process events */ 
                }
                gst_egl_x11_win_run_cb(&render_cb); 

                eglSwapBuffers(eglDisplay, eglSurface);
            }

            gst_egl_x11_win_run_cb(&render_close_cb);
            WHANDLE_MUTEX_LOCK(wHandle);
            destroy_win(wHandle);
            WHANDLE_MUTEX_UNLOCK(wHandle);
        }else{
            GST_ERROR("Failed to create a window!");
        }

        g_atomic_int_set(&wHandle->thread_state, THREAD_STATE_STOPPED);
    }
    GST_INFO("Exiting mainloop");

    return NULL;
}

gboolean
gst_egl_x11_win_display(GstCudaEglX11Win *wHandle, guintptr parent_win, 
        GstCudaEglX11WinRenderCallback *render_init_cb, 
        GstCudaEglX11WinRenderCallback *render_cb, 
        GstCudaEglX11WinRenderCallback *render_close_cb, 
        int xpos, int ypos, int width, int height)
{
    gboolean ret = FALSE;

    if(wHandle){
        GThread *thread = NULL;
        GError *error = NULL;

        WHANDLE_MUTEX_LOCK(wHandle);
        wHandle->parent_win = parent_win;
        wHandle->xpos = xpos;
        wHandle->ypos = ypos;
        wHandle->width = width;
        wHandle->height = height;
        gst_egl_x11_win_copy_cb(&wHandle->render_cb, render_cb);
        gst_egl_x11_win_copy_cb(&wHandle->render_init_cb, render_init_cb);
        gst_egl_x11_win_copy_cb(&wHandle->render_close_cb, render_close_cb);
        WHANDLE_MUTEX_UNLOCK(wHandle);

        g_atomic_int_set(&wHandle->thread_msg, THREAD_MESSAGE_START);
        thread = g_thread_try_new(GST_EGL_X11_THREAD_NAME, window_mainloop, wHandle, &error);
        if(thread){
            GST_INFO("EGL thread started");

            while(1){
                egl_thread_state thread_state = g_atomic_int_get(&wHandle->thread_state);
                gboolean window_created = FALSE;

                if(THREAD_STATE_RUNNING == thread_state){
                    WHANDLE_MUTEX_LOCK(wHandle);
                    window_created = !!wHandle->win;
                    WHANDLE_MUTEX_UNLOCK(wHandle);
                }

                if(THREAD_STATE_STOPPED == thread_state){
                    GST_WARNING("EGL X11 thread stopped!");
                    ret = FALSE;
                    break;
                }

                if(window_created){
                    GST_INFO("EGL X11 window created");
                    break;
                }

                g_usleep(1000);
            }

            ret = TRUE;
        }else{
            GST_INFO("Could not start EGL thread!");
            if(error){
                if(error->message){
                    GST_INFO("Error: %s", error->message);
                }
                g_error_free(error);
            }
        }

        WHANDLE_MUTEX_LOCK(wHandle);
        wHandle->thread = thread;
        WHANDLE_MUTEX_UNLOCK(wHandle);
    }else{
        GST_ERROR("Bad argument, can't display windows!");
    }

    return ret;
}

void
_gst_egl_x11_win_close(GstCudaEglX11Win *wHandle)
{
    egl_thread_state current_state = g_atomic_int_get(&wHandle->thread_state);
    g_atomic_int_set(&wHandle->thread_msg, THREAD_MESSAGE_STOP);

    if(wHandle->thread && (THREAD_STATE_NOT_STARTED != current_state)){
        g_thread_join(wHandle->thread);
    } 
    wHandle->thread = NULL;
    wHandle->thread_state = THREAD_STATE_NOT_STARTED;
    wHandle->thread_msg = THREAD_MESSAGE_NONE;

    destroy_win(wHandle);
}

void
gst_egl_x11_win_close(GstCudaEglX11Win *wHandle)
{
    if(wHandle){
        WHANDLE_MUTEX_LOCK(wHandle);
        _gst_egl_x11_win_close(wHandle);
        WHANDLE_MUTEX_UNLOCK(wHandle);
    }else{
        GST_ERROR("Bad argument!");
    } 
}

GstCudaEglX11Win *
gst_egl_x11_win_create(void)
{
    GstCudaEglX11Win *wHandle = NULL;

    gst_egl_x11_win_init();

    wHandle = g_new0(GstCudaEglX11Win, 1);

    if(wHandle){
        g_mutex_init(&wHandle->mutex);
        wHandle->display = NULL;
        wHandle->win = 0;
        wHandle->eglDisplay = EGL_NO_DISPLAY;
        wHandle->eglSurface = EGL_NO_SURFACE;
        wHandle->eglContext = EGL_NO_CONTEXT;
        wHandle->thread = NULL;
        g_atomic_int_set(&wHandle->thread_state, THREAD_STATE_NOT_STARTED);
        g_atomic_int_set(&wHandle->thread_msg, THREAD_MESSAGE_NONE);

        if(!create_win_context(wHandle)){
            g_free(wHandle);
            wHandle = NULL;
        }
    }else{
        GST_ERROR("Failde to allocate memory!");
    }

    return wHandle;
}

void 
gst_egl_x11_win_destroy(GstCudaEglX11Win *wHandle)
{
    if(wHandle){
        WHANDLE_MUTEX_LOCK(wHandle);
        _gst_egl_x11_win_close(wHandle);

        if(EGL_NO_DISPLAY != wHandle->eglDisplay){
            eglTerminate(wHandle->eglDisplay);
            wHandle->eglDisplay = EGL_NO_DISPLAY;
        }

        if(wHandle->display){
            XCloseDisplay(wHandle->display); 
            wHandle->display = NULL;
        }
        WHANDLE_MUTEX_UNLOCK(wHandle);

        g_free(wHandle);
    }
}
