#ifndef __GST_CUDA_BUFFERPOOL__
#define __GST_CUDA_BUFFERPOOL__

#include <gst/gst.h>
#include <gst/video/gstvideopool.h>

G_BEGIN_DECLS

#define GST_TYPE_CUDA_BUFFERPOOL \
    (gst_cuda_bufferpool_get_type())
#define GST_IS_CUDA_BUFFERPOOL(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_CUDA_BUFFERPOOL))
#define GST_IS_CUDA_BUFFERPOOL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_CUDA_BUFFERPOOL))
#define GST_CUDA_BUFFERPOOL_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_CUDA_BUFFERPOOL, GstCudaBufferpoolClass))
#define GST_CUDA_BUFFERPOOL(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_CUDA_BUFFERPOOL, GstCudaBufferpool))
#define GST_CUDA_BUFFERPOOL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_CUDA_BUFFERPOOL, GstCudaBufferpoolClass))


#define GST_CUDA_BUFFERPOOL_MIN_BUFFERS 2
#define GST_CUDA_BUFFERPOOL_MAX_BUFFERS 0 // 0 - unlimited

#define GST_BUFFER_POOL_OPTION_CUDA_VIDEO_META "GstBufferPoolOptionCudaVideoMeta"

typedef struct _GstCudaBufferpool GstCudaBufferpool;
typedef struct _GstCudaBufferpoolClass GstCudaBufferpoolClass;

struct _GstCudaBufferpool
{
    GstBufferPool bufferpool;
    GstVideoInfo  info;

    gboolean add_videometa;
};

struct _GstCudaBufferpoolClass
{
    GstBufferPoolClass parent_class;
};

GType gst_cuda_buffer_pool_get_type(void);
GstBufferPool *gst_cuda_bufferpool_new(void);

G_END_DECLS

#endif /* __GST_CUDA_BUFFERPOOL__ */
